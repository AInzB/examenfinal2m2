﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NETProjectTutorial
{
    public partial class FrmVentas : Form
    {
        private DataSet dsVentas;
        private BindingSource bsVentas;

        public DataSet DsVentas
        {
            get
            {
                return dsVentas;
            }

            set
            {
                dsVentas = value;
            }
        }

        public FrmVentas()
        {
            InitializeComponent();
            bsVentas = new BindingSource();
        }

        private void btnSalir_Click(object sender, EventArgs e)
        {
            Dispose();
        }

        private void FrmVentas_Load(object sender, EventArgs e)
        {
            bsVentas.DataSource = DsVentas;
            bsVentas.DataMember = DsVentas.Tables["Ventas"].TableName;
            dgvVentas.DataSource = bsVentas;
            dgvVentas.AutoGenerateColumns = true;
        }

        private void limpiar()
        {
            double precio = 0, total = 0;
            int cantidad = 0;

            DataTable dtVentas = dsVentas.Tables["Ventas"];
            int dataCount = dtVentas.Rows.Count;
            DataRow[] drVentas = dtVentas.Select();
            dsVentas.Tables["Ventas"].Rows.Clear();

            foreach(DataRow dr in drVentas)
            {
                DataRow[] drVentasAux =
                dtVentas.Select(String.Format("Factura = {0}",
                dr["Producto"]));

                if(drVentasAux.Length > 1)
                {
                    DataRow drVentaSuma = dsVentas.Tables["Ventas"].NewRow();
                    foreach (DataRow drA in drVentasAux)
                    {
                        precio = (double)drA["Precio"];
                        cantidad += (int)drA["Cantidad"];

                        drVentaSuma["Producto"] = drA["Producto"];
                        drVentaSuma["Codigo"] = drA["Codigo"];
                        drVentaSuma["Descripcion"] = drA["Descripcion"];
                    }
                    total = precio * cantidad;

                    drVentaSuma["Cantidad"] = cantidad;
                    drVentaSuma["Precio"] = precio;
                    drVentaSuma["Total"] = total;

                    dsVentas.Tables["Ventas"].Rows.Add(drVentaSuma);
                }
                else
                {
                    dsVentas.Tables["Ventas"].Rows.Add(dr);
                }

            }

        }


    }
}
