﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NETProjectTutorial.entities
{
    class Ventas
    {
        private int codigo;
        private DetalleFactura dFact;
        private double total;

        public Ventas(int codigo, DetalleFactura dFact, double total)
        {
            this.codigo = codigo;
            this.dFact = dFact;
            this.total = total;
        }

        public int Codigo
        {
            get
            {
                return codigo;
            }

            set
            {
                codigo = value;
            }
        }

        public double Total
        {
            get
            {
                return total;
            }

            set
            {
                total = value;
            }
        }

        internal DetalleFactura DFact
        {
            get
            {
                return dFact;
            }

            set
            {
                dFact = value;
            }
        }
    }
}
